# Zebradex

Un [pokedex](https://fr.wikipedia.org/wiki/Pok%C3%A9dex) pour les situations qui peuvent être rencontrées en tant qu'arbitre (parfois appelé « zèbre ») de roller derby. Attrappez les tous !


## Description
Le zebradex est une mini-encyclopédie de situations que l'on peut rencontrer en arbitrant du roller derby. À l'instar du pokédex où on recense tous les pokémons qu'on a attrappé avec pour objectif de tous les attrapper, le zebradex permet de recenser les situations dans lesquelles on s'est trouvé en tant qu'arbitre.

Ce projet est à destination des arbitres de roller derby, et en particulier de celleux au début de leur carrière arbitrale. Les objectifs de ce projet sont multiples :

- Inciter les arbitres à tester plein de postes différents.
- Familiariser les arbitres débutants avec des situations d'arbitrage insolites, et leur donner plus d'assurance dans la gestion de ces situations.
- Donner envie d'arbitrer.

Le zebradex en tant que tel sera un livret au format A5. Il contiendra des situations pour chaque poste d'arbitre, et un emplacement en face de chaque situation pour mettre un coup de tampon. Il pourra être distribué à l'issue des bootcamps d'arbitrage. 

## Contribuer au projet
### Avec une idée précise en tête
Vous avez déjà rencontré une situation insolite en tant qu'arbitre et vous voudriez qu'elle soit ajoutée au zebradex ? Vous trouvez qu'une des situations du zebradex est mal catégorisée ? Vous voudriez aider à étoffer la liste des situations pour un poste ?

Choisissez un mode de contact :
- On se connait ? Tu sais comment me contacter, viens m'en parler !
- Vous pouvez envoyer un mail à cette adresse et il sera ajouté à la [liste des tickets du projet](https://framagit.org/roller-derby/zebradex/-/issues) : gitlab-incoming+roller-derby-zebradex-100366-bx2ggfwyjsio1jubwwubp8u0m-issue@framagit.org
- Vous pouvez venir me parler sur [Mastodon](https://framapiaf.org/@Moutmout).
- Vous pouvez demander l'accès pour contribuer directement aux différents tickets. 

### Avec l'envie d'aider
Vous avez envie de participer, mais ne savez pas trop comment ?

Allez dans la section « Tickets » pour voir la liste des trucs à faire. Chaque ticket représente une tâche. Ils sont divisés en deux grandes catégories (que vous pouvez retrouver dans la section « Tableaux ») : 
- « considérations générales » : pour tout ce qui touche au projet dans son ensemble.
- « poste arbitral » : pour établir la liste des situations pour un poste donné.

Certains tickets ont des étiquettes supplémentaires. Vous pouvez trier les tickets par étiquette pour trouver ceux qui vous intéressent le plus. Par exemple, si vous êtes doué avec les mots, vous pouvez regarder les tickets avec l'étiquette « vocabulaire » et m'aider à trouver quels termes utiliser.

## Auteurs

- Moutmout (aka Blocktopus)

## License
Ce projet est publié sous une license CC-BY-SA-NC 4.0.
